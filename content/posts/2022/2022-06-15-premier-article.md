---
title: "Mon Premier Article incroyable !"
date: 2022-06-14T11:00:03+02:00
draft: false
---

# Utiliser git et gitlab pour publier un site web

## Git, pour quoi faire? 

Au délà de la gestion de documents textuels, Git et plus particulièrement GitLab, couplés à l’utilisation du langage markdown et de générateur de sites statiques (SSG), sont de formidables pour mettre en place et diffuser, de façon collaborative si nécessaire, simplement et rapidement un site web. Les usages sont multiples :

- Documentation
- Site personnel
- CV

![Logo doranum représentat un nuage bleu et une loupe orange. Le logo signifie "Données de la recherche : apprentissage numérique"](../doranum.png "logo doranum") 
